extends Node2D

signal game_loaded_signal

var next_scene = null

var player_location = Vector2.ZERO
var player_direction = Vector2.ZERO
var file = null

enum TransitionType { NEW_SCENE, PARTY_SCREEN, MENU_ONLY, TITLE}
var transition_type = TransitionType.TITLE

func _ready():
	VisualServer.set_default_clear_color(Color(0,0,0,1.0))
	emit_signal("game_loaded_signal")


func transition_to_party_screen():
	$ScreenTransition/AnimationPlayer.play("FadeToBlack")
	transition_type = TransitionType.PARTY_SCREEN
	
func transition_exit_party_screen():
	$ScreenTransition/AnimationPlayer.play("FadeToBlack")
	transition_type = TransitionType.MENU_ONLY

func transition_to_scene (new_Scene:String, spawn_location:Vector2, spawn_direction:Vector2):
	next_scene = new_Scene
	player_location = spawn_location
	player_direction = spawn_direction
	transition_type = TransitionType.NEW_SCENE
	$ScreenTransition/AnimationPlayer.play("FadeToBlack")
	
func transition_to_game():
	file = File.new()
	if file.file_exists("user://save.dat"):
		var error = file.open("user://save.dat", File.READ)
		if error == OK:
			$ScreenTransition/AnimationPlayer.play("FadeToBlack")
	else:
		next_scene = "res://World/PalletTown/PlayerHomeFloor1.tscn"
		player_location = Vector2(144,80)
		player_direction = Vector2(0,1)
		transition_type = TransitionType.NEW_SCENE
		$ScreenTransition/AnimationPlayer.play("FadeToBlack")
				
		


func finished_fading ():
	match transition_type:
		TransitionType.NEW_SCENE:
			if $CurrentScene.get_child(0) != null:
				$CurrentScene.get_child(0).queue_free()
			$CurrentScene.add_child(load(next_scene).instance())
			
			var player = Utils.get_player()
			player.set_spawn(player_location, player_direction)
		TransitionType.PARTY_SCREEN:
			$Menu.load_party_screen()
		TransitionType.MENU_ONLY:
			$Menu.unload_party_screen()
		TransitionType.TITLE:
			$CurrentScene.add_child(load(next_scene).instance())
	
	$ScreenTransition/AnimationPlayer.play("FadeToNormal")
