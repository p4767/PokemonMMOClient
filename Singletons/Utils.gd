extends Node

var loaded = true

var username


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func get_current_scene_level():
	return get_node("/root/SceneManager/CurrentScene").get_children().back()

func get_current_scene():
	return get_node("/root/SceneManager/CurrentScene")

func get_player():
	return get_node("/root/SceneManager/CurrentScene").get_children().back().find_node("Player")
	
func get_other_players():
	return get_node("/root/SceneManager/CurrentScene").get_children().back().find_node("OtherPlayers")
	
func get_scene_manager():
	return get_node("/root/SceneManager")
	
func load_json_file(path):
	"""Loads a JSON file from the given res path and return the loaded JSON object."""
	var file = File.new()
	file.open(path, file.READ)
	var text = file.get_as_text()
	var result_json = JSON.parse(text)
	if result_json.error != OK:
		print("[load_json_file] Error loading JSON file '" + str(path) + "'.")
		print("\tError: ", result_json.error)
		print("\tError Line: ", result_json.error_line)
		print("\tError String: ", result_json.error_string)
		return null
	var obj = result_json.result
	return obj


func save_game():
	#collect data
	var player = get_player()
	var position: Vector2 = player.position
	var scene: String = get_current_scene_level().filename
	
	var data = {
		"position": position,
		"scene": scene
	}
	
	#save to file
	var file = File.new()
	file.open("user://save.dat",File.WRITE)
	file.store_var(data)
	file.close()
	
func load_game():
	
	var file = File.new()
	if file.file_exists("user://save.dat"):
		var error = file.open("user://save.dat", File.READ)
		if error == OK:
			loaded = false
			var player_data : Dictionary = file.get_var()
			file.close()
			
			
			var scene_manager = get_scene_manager()
			var current_scene = get_current_scene()
			var current_scene_level:Node2D = get_current_scene_level()
			var loaded_scene:String = player_data.get("scene")
			
			current_scene_level.queue_free()
			
			var scene = load (loaded_scene)
			var scene_instance = scene.instance()
			current_scene.add_child(scene_instance)
			
			var player = get_player()
			player.position = player_data.get("position")
			loaded = true	
	
