extends Node2D

export(String, FILE) var file

var dlgExhausted = false
var dlg = null
onready var player = Utils.get_player()
onready var scene_manager = Utils.get_scene_manager()
onready var dlgBox = player.get_node("DlgControl")
onready var dlgText = player.get_node("DlgControl/NinePatchRect/RichTextLabel")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass
		
func _unhandled_input(event):
	if event.is_action_pressed("z"):
		if player.interactable_ray.is_colliding() and !dlgExhausted:
			setText()
			player.set_physics_process(false)
			dlgBox.visible = true
			dlgExhausted = true
		elif player.interactable_ray.is_colliding() and dlgExhausted:
			setText()
			player.set_physics_process(true)
			dlgBox.visible = false
			if dlg["repeatable"]:
				dlgExhausted = false
					
func setText():
	if file !="":
		print(file)
		dlg = Utils.load_json_file(file)
		dlgText.text = dlg["text"]
	else:
		dlgExhausted = true
	
