extends Node2D

var player_spawn = preload("res://Player/PlayerTemplate.tscn")
var last_world_state = 0

func SpawnNewPlayer(player_id, spawn_position):
	if get_tree().get_network_unique_id() == player_id:
		pass
	else:
		if not Utils.get_other_players().has_node(str(player_id)):
			var new_player = player_spawn.instance()
			new_player.position = spawn_position
			new_player.name = str(player_id)
			Utils.get_other_players().add_child(new_player)
		else:
			pass
			
func DespawnPlayer(player_id):
	if get_child(0).has_node("OtherPlayers"):
			get_child(0).get_node("OtherPlayers/" + str(player_id)).queue_free()
	else:
		pass

func UpdateWorldState(world_state):
	#buffer
	#Interpolation
	#Extrapolation
	#Rubber banding
	if world_state["T"] > last_world_state:
		last_world_state = world_state["T"]
		world_state.erase("T")
		world_state.erase(get_tree().get_network_unique_id())
		for player in world_state.keys():
			if Utils.get_other_players() != null and Utils.get_other_players().has_node(str(player)):
				get_node(str(Utils.get_other_players().get_path()) +"/"+ str(player)).MovePlayer(world_state[player]["P"])
			else:
				print("spawning player")
				SpawnNewPlayer(player, world_state[player]["P"])
